
require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'


//Vue Chat Scroll
import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll);

//Vue Toaster
import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
Vue.use(Toaster, {timeout: 3000})


Vue.component('message-component', require('./components/MessageComponent.vue').default);



const app = new Vue({
    el: '#app',
    data : {
        message : '',
        chat : {
            message : [],
            user    : [],
            color   : [],
            time    : []
        },
        typing  : '',
        numberOfUsers : 0
    },

    watch: {
        message(){
            Echo.private('chat')
                .whisper('typing', {
                    message: this.message
                });
        }
    },

    methods : {
        send(){
            if (this.message.length != 0){
                this.chat.message.push(this.message)
                this.chat.user.push("You")
                this.chat.color.push("success")
                this.chat.time.push(this.getTime())
                axios.post('/send-message', {
                    message: this.message
                })
                    .then(response => {
                        console.log(response);
                        this.message = '';
                    })
                    .catch( error => {
                        console.log(error);
                    });
            }
        },
        getTime(){
            let time = new Date();

            let hours = time.getHours();
            let minutes = time.getMinutes();
            let ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12;
            minutes = minutes < 10 ? '0' + minutes : minutes;

            let day, month;
            switch(time.getDay()) {
                case 1:
                    day = 'Sun';
                    break;
                case 2:
                    day = 'Mon';
                    break;
                case 3:
                    day = 'Tue';
                    break;
                case 4:
                    day = 'Wed';
                    break;
                case 5:
                    day = 'Thu';
                    break;
                case 6:
                    day = 'Fri';
                    break;
            }
            switch(time.getMonth()) {
                case 0:
                    month = 'Jan';
                    break;
                case 1:
                    month = 'Feb';
                    break;
                case 2:
                    month = 'Mar';
                    break;
                case 3:
                    month = 'Apr';
                    break;
                case 4:
                    month = 'May';
                    break;
                case 5:
                    month = 'Jun';
                    break;
                case 6:
                    month = 'Jul';
                    break;
                case 7:
                    month = 'Aug';
                    break;
                case 8:
                    month = 'Sep';
                    break;
                case 9:
                    month = 'Oct';
                    break;
                case 10:
                    month = 'Nov';
                    break;
                case 11:
                    month = 'Dec';
                    break;
            }
            return hours + ':' + minutes + ampm + ' ' + day + ', ' + time.getDate() + '-' + month + '-' + time.getFullYear();
        }
    },
    mounted(){
        Echo.private('chat')
            .listen('ChatEvents', (e) => {
                this.chat.message.push(e.message)
                this.chat.user.push(e.user)
                this.chat.color.push("danger")
                this.chat.time.push(this.getTime())
            })
            .listenForWhisper('typing', (e) => {
                if (e.message != '')
                    this.typing = 'typing...'
                else
                    this.typing = ''
            });

        Echo.join(`chat`)
            .here((users) => {
                this.numberOfUsers = users.length - 1;
            })
            .joining((user) => {
                console.log(user)
                this.numberOfUsers += 1;
                this.$toaster.success(user.name + ' has joined the chat room')
            })
            .leaving((user) => {
                console.log(user)
                this.numberOfUsers -= 1;
                this.$toaster.error(user.name + ' has leaved the chat room')
            });
    }
});
