<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chat Room</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style>
        .list-group{
            overflow-y: scroll;
            height: 300px;
            border: 1px solid #3490dc;
            border-bottom: 0;
            background: #fff;
            border-radius: 0;
        }
        .form-control{
            border-radius: 0 !important;
            border: 1px solid #3490dc;
        }
        .list-group{

        }
    </style>
</head>
<body>
    <div id="app" class="container mt-5">
        <div class="row">
            <div class="col-md-3 col-sm-0"></div>
            <div class="col-md-6 col-sm-12">
                <li class="list-group-item active">
                    <div class="float-left">
                        <span class="badge badge-dark" style="font-size: 20px; font-weight: bold;">Chat Room</span>
                    </div>
                    <div style="text-align: right;">Online :
                        <span class="badge badge-success" style="font-weight: bold; font-size: 16px;">@{{ numberOfUsers }}</span>
                    </div>
                </li>
                <div class="badge badge-pill badge-success">@{{ typing }}</div>
                <ul class="list-group" v-chat-scroll>
                    @if(count($user_message) > 0)
                        @foreach($user_message as $value)
                            @if($value->user_id == Auth::id())
                                <div style="margin-top: 2%;">
                                    <li class="list-group-item list-group-item-success"  style="font-weight: bold; border-radius: 0;">
                                        {{ $value->body }}
                                    </li>
                                    <div>
                                        <small class="badge badge-success float-md-right" style="border-radius: 0;">
                                            {{ ($value->user_id == Auth::id()) ? 'You' : $value->user_name }}
                                        </small>
                                        <small class="badge list-group-item-success float-md-left" style="border-radius: 0;">
                                            {{ Carbon\Carbon::parse($value->created_at)->format('g:iA D, d-M-Y') }}
                                        </small>
                                    </div>
                                </div>
                            @else
                                <div style="margin-top: 2%;">
                                    <li class="list-group-item list-group-item-danger"  style="font-weight: bold; border-radius: 0;">
                                        {{ $value->body }}
                                    </li>
                                    <div>
                                        <small class="badge badge-danger float-md-right" style="border-radius: 0;">
                                            {{ ($value->user_id == Auth::id()) ? 'You' : $value->user_name }}
                                        </small>
                                        <small class="badge list-group-item-danger float-md-left" style="border-radius: 0;">
                                            {{ Carbon\Carbon::parse($value->created_at)->format('g:iA D, d-M-Y') }}
                                        </small>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                    <message-component
                        v-for   = "(value,index) in chat.message"
                        :key    = value.index
                        :color  = chat.color[index]
                        :time   = chat.time[index]
                        :user   = chat.user[index]>
                        @{{ value }}
                    </message-component>
                </ul>
                <input type="text" class="form-control" placeholder="Type your Message here" v-model="message" @keyup.enter="send">
            </div>
            <div class="col-md-3 col-sm-0"></div>
        </div>

        <div class="row mt-3">
            <div class="col-md-3 col-sm-0"></div>
            <div class="col-md-6 col-sm-12">
                <a class="btn btn-danger btn-sm btn-block" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
            <div class="col-md-3 col-sm-0"></div>
        </div>

    </div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
