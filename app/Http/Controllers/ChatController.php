<?php

namespace App\Http\Controllers;

use App\ChatMessage;
use App\Events\ChatEvents;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ChatController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $user = User::find(Auth::id());

        $user_message = ChatMessage::where('created_at', '>=', Carbon::parse($user->created_at)->toDateTimeString())->get();

        return view('chat', compact('user_message'));
    }

    public function sendMessage(Request $request){
        $user = User::find(Auth::id());
        event(new ChatEvents($user, $request->message));
        $this->store($request->message);
    }

    public function store($message){
        $chat_message = new ChatMessage;
        $chat_message->user_id   = Auth::id();
        $chat_message->user_name = Auth::user()->name;
        $chat_message->body      = $message;
        $chat_message->save();
    }
}
